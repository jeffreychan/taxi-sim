from __future__ import absolute_import
from __future__ import print_function
from multiprocessing import Pool

import os
from pydoc import describe
import sys
import optparse

# from jsonschema import draft201909_format_checker

# we need to import python modules from the $SUMO_HOME/tools directory

if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")

from sumolib import checkBinary  # noqa
import traci  # noqa
import sumolib


from turtle import pos
from unittest import result
from grpc import server
# from tokenize import Double

# from pyrsistent import VT

from shapely.geometry import Polygon, LineString, Point
from sympy import EX, li, rf, rootof, true
import json
import pandas as pd
import re
import subprocess


import geopandas as gpd
import numpy as np
from packages.decision import Model2, RebalanceModel, DispatchModel
# from packages.predict_func import predict_model
import time
import pickle
import xml.etree.ElementTree as ET
from packages.logprinter import LogPrinter
from packages.perfectpredict import PerfectPred
from packages.router import Router
from packages.rwlock import RWLock
import copy
import random
import datetime


PREDTIME = 10 * 60

STEPTIME = 30

REBALANCETIME = PREDTIME

REBALANCETASKTIME = 2 * 60

STOPFLAG = 3

FLEETSZ = 350

READREGIONCOST = False

SAVEREGIONCOST = not READREGIONCOST

WITHREBALANCE = True

HIGHLIGHTSZ = 40

DSAGGREGATEWINDOW = PREDTIME

ROUTETIMEGETMODE = 1

SIMTIME = 14400

DISPATCHALGO = 'GreedyClosest'

GREEN = (0,188,0,255)
RED = (255,0,0,255)
BROWN = (149,82,0,255)

BIGINT = 9999999999

# 导入设置参数
with open(r'data/settings/parameters.json', 'r') as paraFile:
    paras:dict = json.load(paraFile)
    PREDTIME = paras["PredictWindow"]
    STEPTIME = paras["StepTime"]
    REBALANCETASKTIME = paras["RebalanceTaskWindow"]
    FLEETSZ = paras["FleetSize"]
    WITHREBALANCE = paras["WithRebalance"]
    SIMTIME = paras['SimulationTime']
    STARTTIME = paras['StartTime']
    ENDTIME=paras['EndTime']
    if paras['DispatchAlgo'] != 'Hungarian' and paras['DispatchAlgo'] != 'MinCostFlow' \
            and paras['DispatchAlgo'] != 'GreedyClosest' and paras['DispatchAlgo'] != 'TestShared' \
            and paras['DispatchAlgo'] != 'InsertionHeuristic':
        raise Exception('data/settings/parameters.json')
    else:
        DISPATCHALGO = paras['DispatchAlgo']
    # if paras['DispatchAlgo'] == 'InsertionHeuristic':
    #     if STARTTIME != '12:00:00' and SIMTIME >= 5 * 3600:
    #         raise Exception('data/settings/parameters.json: InsertionHeuristic算法只适用于StartTime为12:00:00且SIMTIME<5小时的情况！')
    paraFile.close()

h,m,s=map(int,STARTTIME.split(':'))
StartTime=datetime.timedelta(hours=h,minutes=m,seconds=s)
h,m,s=map(int,ENDTIME.split(':'))
EndTime=datetime.timedelta(hours=h,minutes=m,seconds=s)
if int(StartTime.total_seconds())>0:
    tree=ET.parse(r'data/sumo_inputs/convert-1-personDemandtrips.xml.xml')
    root=tree.getroot()
    with open(r'data/sumo_inputs/a.xml','w') as f:
        f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        f.write('<routes>\n\t')
        for person in root.findall('person'):
            
            depart=person.attrib['depart']
            if int(EndTime.total_seconds())>int(depart):
            # id=person.attrib['id']
                depart=str(int(depart)-int(StartTime.total_seconds()))
                person.set('depart',depart)
                if int(person.attrib['depart'])<0:
                    id_0=person.attrib['id']
                else:
                    id=person.attrib['id']
                    id=str(int(id)-int(id_0)-1)
                    person.set('id',id)
                    f.write(ET.tostring(person,encoding='utf-8').decode('utf-8'))

        f.write('</routes>')
    data=pd.read_csv(r'data/pre_cal/24h0107_end.csv')
    data=data.loc[(data['pickup_datetime']>=int(StartTime.total_seconds()))&(data['pickup_datetime']<int(EndTime.total_seconds()))]
    data.pickup_datetime=data.pickup_datetime-int(StartTime.total_seconds())
    
    data.to_csv(r'data/pre_cal/24hour_end_1.csv',index=False)
else:
    df1=pd.read_csv(r'data/pre_cal/24h0107_end.csv')
    df1.to_csv(r'data/pre_cal/24hour_end_1.csv',index=False)
    tree=ET.parse(r'data/sumo_inputs/convert-1-personDemandtrips.xml.xml')
    tree.write(r'data/sumo_inputs/a.xml')


class EnrouteInfo(object):

    def __init__(self, desArea:str, estArrivalTime:float) -> None:
        self._desArea = desArea                  # 最终到达区域（id）
        self._estArrivalTime = estArrivalTime    # 估计到达时间（s)
    
    @property
    def desArea(self):
        return self._desArea

    @property
    def estArrivalTime(self):
        return self._estArrivalTime

class PoolingTaskInfo(EnrouteInfo):
    # 只包括serving车辆
    _poolingRecords = {}
    
    # 关闭的订单
    _closedResRecords = {}
    # 乘客到所在订单的映射
    _persons2Res = {}
    
    _pairDict = {}

    _pairMissedEdges = set()
    
    _lockTaskCache = RWLock()

    _errorVehs = set()
    
    
    def getPoolingRecordsLen():
        return len(PoolingTaskInfo._poolingRecords) 
    
    def getPoolingRecordKeys():
        return PoolingTaskInfo._poolingRecords.keys()
    def getPoolingRecordItems():
        lst = list(PoolingTaskInfo._poolingRecords.items())
        return copy.deepcopy(lst)
    
    def getPoolingRecordValue(key):
        return copy.deepcopy(PoolingTaskInfo._poolingRecords[key])

    def getClosedResRecord(key):
        return copy.deepcopy(PoolingTaskInfo._closedResRecords[key])

    def getPoolingRecords():
        return copy.deepcopy(PoolingTaskInfo._poolingRecords)
    
    

    @property
    def closedResRecords() -> dict:
        return PoolingTaskInfo._closedResRecords
    
    # @property
    # def pairDict() -> dict:
    #     return PoolingTaskInfo._pairDict
    
    def getEdgePairTimeCost(fedge:str, tedge:str):
        if fedge == tedge:
            return 0.0
        else:
            key = fedge + '_' + tedge
            if key in PoolingTaskInfo._pairDict:
                return PoolingTaskInfo._pairDict[fedge + '_' + tedge]
            else:
                # raise Exception('getEdgePairTimeCost')
                if fedge + '_' + '1535' not in PoolingTaskInfo._pairDict:
                    if fedge not in PoolingTaskInfo._pairMissedEdges:
                        PoolingTaskInfo._pairMissedEdges.add(fedge)
                        with open(r'data/pair_error.txt', 'a') as outf:
                            outf.write(f'%s\n' % (fedge))
                elif tedge + '_' + '1535' not in PoolingTaskInfo._pairDict:
                    if tedge not in PoolingTaskInfo._pairMissedEdges:
                        PoolingTaskInfo._pairMissedEdges.add(tedge)
                        with open(r'data/pair_error.txt', 'a') as outf:
                            outf.write(f'%s\n' % (tedge))
                return float('inf')
                # stg = traci.simulation.findRoute(fromEdge=fedge, toEdge=tedge, vType='taxi')
                # if len(stg.edges) == 0:
                #     PoolingTaskInfo._pairDict[key] = float('inf')
                #     return float('inf')
                # else:
                #     PoolingTaskInfo._pairDict[key] = stg.travelTime
                #     return stg.travelTime


    def _readEdgePairs():
        # 读取
        # for pair, cost in sumolib.xml.parse_fast(r'data/pre_cal/edges_pair_graph_12-17.xml', 'pair', ['id', 'cost']):
        #     PoolingTaskInfo._pairDict[pair] = float(cost)
        with open(r'data/pre_cal/edges_pair_graph_all.pkl', 'rb') as f:
            PoolingTaskInfo._pairDict = pickle.load(f)

    def init():
        PoolingTaskInfo._lockTaskCache.w_acquire()
        if DISPATCHALGO == 'InsertionHeuristic':
            PoolingTaskInfo._readEdgePairs()
            with open(r'data/pair_error.txt', 'w') as outf:
                outf.close()
            with open(r'data/errors.txt', 'w') as outf:
                outf.close()
    
    def stepBegin():
        # 每一步都要调用（通过len(poolingRecords)检查是否和主模块的记录一样
        servingFleet = set(traci.vehicle.getTaxiFleet(-1)) - set(traci.vehicle.getTaxiFleet(0))
        allFleet = set(traci.vehicle.getTaxiFleet(-1))

        # 清除掉跑出路网的车
        for taxiId in set(PoolingTaskInfo._poolingRecords.keys()):
            if taxiId not in allFleet:
                PoolingTaskInfo._poolingRecords.pop(taxiId) 
        
        for taxiId in set(PoolingTaskInfo._poolingRecords.keys()):   # 不转成set循环会有bug
            # 得到这辆出租车当前任务队列中的所有顾客id
            currentCustomersSet = set()
            x = traci.vehicle.getParameter(taxiId, 'device.taxi.currentCustomers')
            x = x.split()
            for personId in x:
                currentCustomersSet.add(str(personId))

            # 根据currentCustomersSet，对_myTaskQueueCache进行更新
            taskQueue = PoolingTaskInfo._poolingRecords[taxiId].taskQ
            # last step的订单乘客集合
            oldCustomersSet = set()
            for resId in set(taskQueue):
                oldCustomersSet |= set(PoolingTaskInfo._closedResRecords[resId].persons)
            # 找出已完成的订单
            finishedCustomersSet = oldCustomersSet - currentCustomersSet
            assert oldCustomersSet >= currentCustomersSet
            if len(finishedCustomersSet) > 0:
                finishedResIdSet = set()  # 已完成的订单
                for resId in set(taskQueue):
                    personsTup = PoolingTaskInfo._closedResRecords[resId].persons
                    resPersons = set(personsTup)    # debug: 检查resPersons的类型
                    # numFinished = 0
                    if resPersons <= finishedCustomersSet:
                        # 删除已完成的订单记录
                        PoolingTaskInfo._closedResRecords.pop(resId)
                        deletedId = PoolingTaskInfo._persons2Res.pop(personsTup)
                        assert deletedId == resId
                        finishedResIdSet.add(resId)
                
                # 剔除原本任务队列中已完成的订单
                if len(finishedResIdSet) > 0:
                    taskQueue = [resId for resId in taskQueue if resId not in finishedResIdSet]
                    PoolingTaskInfo._poolingRecords[taxiId]._taskQ = taskQueue
                
                assert len(taskQueue) % 2 == 0

                if len(taskQueue) == 0:
                    # 已完成所有任务
                    PoolingTaskInfo._poolingRecords.pop(taxiId)
        
        # assert len(servingFleet) == len(PoolingTaskInfo._poolingRecords)
                
        # 使得所有PoolingTaskInfo对象的_currPos失效
        for vID, info in PoolingTaskInfo._poolingRecords.items():
            info._currPos = -1
        
        # 此时TaskRecord的currPos仍需要更新
        for taxiId in PoolingTaskInfo._poolingRecords.keys():
            onBoardCustomers = set(traci.vehicle.getPersonIDList(taxiId))
            taskQueue = PoolingTaskInfo._poolingRecords[taxiId].taskQ
            # 找到车上乘客所属的订单
            occurResSet = set() 
            for persons, resId in PoolingTaskInfo._persons2Res.items():
                if onBoardCustomers >= set(persons):
                    occurResSet.add(resId)

            foundPos = -1
            for idx, resId in enumerate(taskQueue):
                if resId in occurResSet:
                    occurResSet.remove(resId)
                else:
                    # bugggggg处理
                    if len(occurResSet) != 0:
                        if taxiId not in PoolingTaskInfo._errorVehs:
                            PoolingTaskInfo._errorVehs.add(taxiId)
                        break
                    # idx即id为taxiId的出租车正在执行的任务序号
                    foundPos = idx
                    break
            
            # assert foundPos >= 0
            
            PoolingTaskInfo._poolingRecords[taxiId]._currPos = foundPos
        
        for taxiID in PoolingTaskInfo._errorVehs:
            PoolingTaskInfo._poolingRecords.pop(taxiID)
            with open(r'data/errors.txt', 'a') as outf:
                outf.write(f'Timestep: %f, stepBegin, 出错taxiID：%s\n' 
                % (traci.simulation.getTime() ,taxiID))
                outf.close()
        
        PoolingTaskInfo._errorVehs = set()

        # PoolingTaskInfo的静态变量已更新完毕
        PoolingTaskInfo._lockTaskCache.w_release()

    def stepFinish():
        PoolingTaskInfo._lockTaskCache.w_acquire()

    ### 以下是成员方法
    def __init__(self, taskQueue: list, initPos = 0) -> None:
        # desArea未确定
        super().__init__(None, None)
        self._taskQ = taskQueue
        self._currPos = initPos
    
    # 同时负责bookkeeping和执行接口的调用
    def changeTaskQ(resObj, vID, pickupInsertPos=0, dropoffInsertPos=0) -> bool:
        PoolingTaskInfo._lockTaskCache.w_acquire()
        try:
            if vID in PoolingTaskInfo._poolingRecords:
                isEmptyCar = False
                info:PoolingTaskInfo  = PoolingTaskInfo._poolingRecords[vID]
                # pickupInsertPos, dropoffInsertPos要符合一定的规则
                if pickupInsertPos > dropoffInsertPos or dropoffInsertPos > len(info.taskQ) \
                            or pickupInsertPos < info.currPos:
                    raise Exception('changeTaskQ')
                tempTaskQueue = copy.deepcopy(info._taskQ)  # 插入尝试
                currentPos = info._currPos
            else:
                # 这是一辆空车
                isEmptyCar = True
                # pickupInsertPos, dropoffInsertPos要符合一定的规则
                pickupInsertPos = 0
                dropoffInsertPos = 0
                tempTaskQueue = []  # 插入尝试
                currentPos = 0
            
            # 先插入pickupInsertPos
            tempTaskQueue.insert(pickupInsertPos, resObj.id)
            dropoffInsertPos += 1
            tempTaskQueue.insert(dropoffInsertPos, resObj.id)

            ### 检查插入后连通性
            odPairs = set()

            pickupFormerPos = pickupInsertPos - 1
            pickupLatterPos = pickupInsertPos + 1
            if pickupFormerPos == currentPos - 1:
                # 起点插在了第一个订单前面（或者这个订单派给了一辆空车）
                # buggy
                if isEmptyCar:
                    # 如果这辆车是空车（闲车）
                    stops = traci.vehicle.getStops(vID)
                    assert len(stops) == 1
                    originalStopData = stops[0]
                    vehNowEdge = traci.lane.getEdgeID(originalStopData.lane)
                    pickupFormerEdge = vehNowEdge
                else:
                    if traci.vehicle.getStopState(vID) == 0:
                        # 一般是正常状态（车在路线上跑）
                        # buggy
                        vehOnEdge = traci.vehicle.getRoadID(vID)
                        if vehOnEdge == '' or vehOnEdge[0] == ':':
                            # laneID = traci.vehicle.getLaneID(vID)
                            route = traci.vehicle.getRoute(vehID=vID)
                            rIdx = traci.vehicle.getRouteIndex(vID)
                            vehOnEdge = route[rIdx]
                            # raise Exception('changeTaskQ')
                        else:
                            # debug
                            print('ok')
                        pickupFormerEdge = vehOnEdge
                    else:
                        # 如果这辆车恰好停靠在路边让乘客上下车
                        with open(r'data/errors.txt', 'a') as outf:
                            outf.write(f'Timestep: %f, changeTaskQ 返回 False, \
                                        原因：这辆车恰好停靠在路边让乘客上下车。\nargs = [resObjID = %s, vID = %s, \
                                        pickupInsertPos = %d, dropoffInsertPos = %d]\n' 
                            % (traci.simulation.getTime() ,resObj.id, vID, pickupInsertPos, dropoffInsertPos))
                            outf.close()
                        return False
            else:
                # 起点前有订单任务
                pickupFormerEdge, _ = PoolingTaskInfo.getTaskPointEdge(tempTaskQ=tempTaskQueue, pos=pickupFormerPos)

            
            pickupEdge = resObj.fromEdge

            if pickupLatterPos == dropoffInsertPos:
                    pickupLatterEdge = resObj.toEdge    # 插入订单的终点紧接着起点
            else:
                pickupLatterEdge, _ = PoolingTaskInfo.getTaskPointEdge(tempTaskQ=tempTaskQueue, pos=pickupLatterPos)

            
            odPairs.add((pickupFormerEdge, pickupEdge))
            odPairs.add((pickupEdge, pickupLatterEdge))

            dropoffFormerPos = dropoffInsertPos - 1
            dropoffLatterPos = dropoffInsertPos + 1
            if dropoffFormerPos == pickupInsertPos:
                dropoffFormerEdge = resObj.fromEdge
            else:
                dropoffFormerEdge, _ = PoolingTaskInfo.getTaskPointEdge(tempTaskQ=tempTaskQueue, pos=dropoffFormerPos)
            
            dropoffEdge = resObj.toEdge

            if dropoffLatterPos == len(tempTaskQueue):
                # 任务队列队尾放下这个新订单
                dropoffLatterEdge = None
            else:
                dropoffLatterEdge, _ = PoolingTaskInfo.getTaskPointEdge(tempTaskQ=tempTaskQueue, pos=dropoffLatterPos)
            
            odPairs.add((dropoffFormerEdge, dropoffEdge))
            if dropoffLatterEdge is not None:
                odPairs.add((dropoffEdge, dropoffLatterEdge))
            
            vType = traci.vehicle.getTypeID(vehID=vID)
            for pair in odPairs:
                # 每个od对都必须相通
                stgRes = traci.simulation.findRoute(fromEdge=pair[0], toEdge=pair[1], vType=vType)
                stgRes_reverse = traci.simulation.findRoute(fromEdge=pair[1], toEdge=pair[0], vType=vType)
                if len(stgRes.edges) == 0 or len(stgRes_reverse.edges) == 0:
                    # 汽车完成这个订单序列或者执行完毕之后“铁定”回不来
                    with open(r'data/errors.txt', 'a') as outf:
                        outf.write(f'Timestep: %f, changeTaskQ 返回 False, \
                                    原因：汽车完成这个订单序列或者执行完毕之后“铁定”回不来。\
                                    \nargs = [resObjID = %s, vID = %s, pickupInsertPos = %d, dropoffInsertPos = %d]\n' 
                            % (traci.simulation.getTime() ,resObj.id, vID, pickupInsertPos, dropoffInsertPos))
                        outf.close()
                    return False
                
            # 执行派单命令
            try:
                traci.vehicle.dispatchTaxi(vID, tempTaskQueue)
            except Exception as ex:
                print(ex)
                exit('dispatchTaxi调用失败')
            
            # 将修改写入字典
            PoolingTaskInfo._poolingRecords[vID] = PoolingTaskInfo(taskQueue=tempTaskQueue, initPos=currentPos)
            PoolingTaskInfo._persons2Res[resObj.persons] = resObj.id
            PoolingTaskInfo._closedResRecords[resObj.id] = resObj
            return True
                     
        finally:
            PoolingTaskInfo._lockTaskCache.w_release()
    
    def getTaskPointEdge(tempTaskQ, pos):
        appearTimes = tempTaskQ[:pos].count(tempTaskQ[pos])
        if appearTimes == 0:
            # 这是一个pickup点
            atPosEdge = PoolingTaskInfo._closedResRecords[tempTaskQ[pos]].fromEdge
        elif appearTimes == 1:
            # 这是一个dropoff点
            atPosEdge = PoolingTaskInfo._closedResRecords[tempTaskQ[pos]].toEdge
        else:
            raise Exception('changeTaskQ')
        return atPosEdge, appearTimes
    
    @property
    def taskQ(self):
        return self._taskQ
    
    @property
    def currPos(self):
        return self._currPos


class RegionProjector(object):
    
    def __init__(self, regions) -> None:
        self._gdf =  gpd.read_file('data/taxi_zones/taxi_zones.shp')
        self._regions = set(regions)
        other_regions=[]
        for i in range(1,264):
            if i not in self._regions:
                other_regions.append(i)
        for i in other_regions:
            self._gdf.drop(i-1,inplace=True)
        self._gdf = self._gdf.to_crs('EPSG:4326')

        self._resultCache = {}

    
    def inWhichRegion(self, x, y, isCache = False):
        if isCache and (x, y) in self._resultCache:
            return self._resultCache[(x,y)]
        series = self._gdf.contains(Point(x, y))
        inRegions = series[series == True]
        if len(inRegions) > 0:
            result = inRegions.index[0] + 1 # region的编号比index大1
            if isCache:
                self._resultCache[(x,y)] = result
            return result   
        else:
            return None
    
    @property
    def regionCentroids(self):
        return self._gdf.centroid
    
    @property
    def regionIDList(self): # 运营region集合
        return self._regions


class Decision(object):

    def __init__(self, service:dict, rebalance:dict) -> None:
        self._service = service
        self._rebalance = rebalance

    @property
    def service(self):
        return self._service

    @property
    def rebalance(self):
        return self._rebalance


class DecisionModel():
    # _rebalance

    def compute(self, vacantFleet:set, openReservations:set, regions:set) -> Decision:
        # np.random.seed(0)
        service = {}
        rebalance = {}
        openReservations = list(openReservations)
        regions = list(regions)
        
        maxBalanced = 5
        balanced = 0

        assert len(regions) > 0
        for vID in vacantFleet:
            if len(openReservations) > 0:
                flag = np.random.randint(0, 2)
                ### debug
                # flag = 0
                if flag == 0:
                    # 随机匹配
                    resID = np.random.choice(openReservations, 1)[0]
                    openReservations.remove(resID)
                    service[vID] = resID 
                else:
                    if balanced == maxBalanced:
                        continue
                    # 随机指派到一个region
                    regionID = np.random.choice(regions, 1)[0]
                    rebalance[vID] = regionID
                    balanced += 1
                    
            elif len(vacantFleet) < 600:
                # 剩下的五分之一车辆再平衡
                flag = np.random.randint(0, 2)
                ### debug
                # flag = 0
                if flag > 0:
                    pass
                else:
                    # 随机指派到一个region
                    if balanced == maxBalanced:
                        continue
                    regionID = np.random.choice(regions, 1)[0]
                    rebalance[vID] = regionID
                    
                    balanced += 1

        
        return Decision(service, rebalance)



class Controller(object):

    def __init__(self) -> None:
        # self._model = Model2(1, 0.1, 2000)       # 决策模型对象
        self._rebalanceModel = RebalanceModel()
        self._dispatchModel = DispatchModel()
        self._servingPool = {}              # 正在执行serive任务的车辆记录
        self._rebalancingPool = {}          # 正在执行rebalance任务的车辆记录
        self._openReservationPool = {}      # 等待匹配的订单
        self._regionDemandRecord = {}       # 记录DSAGGREGATEWINDOW内各个region产生的累计需求数
        self._regionSupplyRecord = {}       # 记录DSAGGREGATEWINDOW内各个region新产生的累计供应数
        self._centralEdges = {}             # region的中心位置对应的路（边）
        self._errorReservationRec = {}      # 记录订单的起点终点是否（双向）连通
        self._outOfRegionBoundTimes = 0     # 需求产生点超出运营区域的次数
        self._regionalCosts  = None
        self._rebalanceLenRank = None
        self._rebalanceTasks = None
        self._net = sumolib.net.readNet(r'data/sumo_inputs/convert-1.net.xml')
        self._lastTimeVacantFleet = set()
        self._lastFleetSZ = 0
        self._logprinter = LogPrinter()
        self._dataToPrint = {'regionDemandSupplyRealtimeInfo': None, 
                                'regionDemandSupplyAggregateInfo': {'startTime':None, 'timeSpan':None, 'regionD':None, 'regionS':None}, 
                                'rebalanceinfo': [], 'servicepredinfo': None, 'taxistatnuminfo': None,
                                'regionDemandSupplyPredInfo': {'startTime':None, 'timeSpan':None, 'regionD':None, 'regionS':None}}
        self._predictor = PerfectPred()
        self._rebalanceWindowRVCnt = 0
        self._rebalanceWindowRVCntObj = 0

        # 区域中心点到边的映射
        with open('data/pre_cal/centralEdges.json', 'r') as centralEdgesFile:
            self._centralEdges:dict = json.load(centralEdgesFile)
        old_keys = set(self._centralEdges.keys())
        for old_key in old_keys:
            new_key = int(old_key)
            self._centralEdges[new_key] = str(self._centralEdges[old_key])
            del self._centralEdges[old_key]

        self._regionProjector = RegionProjector(self._centralEdges.keys())

        # 初始化区域需求量记录
        self._regionDemandRecord = dict.fromkeys(self._regionProjector.regionIDList, 0)
        # 初始化区域供应量到达数记录
        self._regionSupplyRecord = dict.fromkeys(self._centralEdges.keys(), 0) 


        temp_dict = dict.fromkeys(self._centralEdges.keys())
        vehNumEachRegion = int(FLEETSZ / len(self._centralEdges))
        sum = 0
        for idx, (key, value) in enumerate(temp_dict.items()):
            if idx == len(temp_dict) - 1:
                temp_dict[key] = FLEETSZ - sum
            else:
                sum += vehNumEachRegion
                temp_dict[key]=vehNumEachRegion

        

        routes = dict.fromkeys(self._regionProjector.regionIDList)

        # 添加车队起始路线
        for regionID, edgeID in self._centralEdges.items():
            routeID = 'ROUTE' + str(regionID)
            traci.route.add(routeID=routeID, edges=[edgeID])
            routes[regionID] = routeID
        
        vCnt = 0
        # 往每个区域中心点添加车辆
        for regionID in self._regionProjector.regionIDList:
            routeID = routes[regionID]
            regionVehNum = temp_dict[regionID]
            while regionVehNum > 0:
                vCnt += 1
                assert vCnt <= FLEETSZ
                taxiID = 'TAXI' + str(vCnt)    
                traci.vehicle.add(vehID=taxiID, routeID=routeID, typeID='taxi', depart='now')
                # 设置路由模式
                traci.vehicle.setRoutingMode(vehID=taxiID, routingMode=traci.constants.ROUTING_MODE_AGGREGATED)

                regionVehNum -= 1


    def step(self):
        ### 决策间隔为STEPTIME
        now = traci.simulation.getTime()
        if now == SIMTIME:
            return -1
        if now % STEPTIME != 0:
            return 1
        ### 供给信息收集
        print('-------------------' + '第' + str(now) + '步' + '-------------------')
        allFleet = set(traci.vehicle.getTaxiFleet(-1))
        if allFleet != allFleet & set(traci.vehicle.getIDList()):  # TODO manage teleports # noqa
                print("\nVehicle %s is being teleported, skip to next step" %
                      (allFleet- set(traci.vehicle.getIDList())))
                return -2  # if a vehicle is being teleported skip to next step
        begin0 = time.time()
        
        begin = time.time()
        PoolingTaskInfo.stepBegin()
        end = time.time()
        print(f'stepBegin用时：%f s' % (end - begin))
        
        # 空车（包括vacant，rebalancing）集合
        emptyFleet = traci.vehicle.getTaxiFleet(0)

        # 移除掉意外退出路网的车辆记录
        # allFleet = set(traci.vehicle.getTaxiFleet(-1))
        nowFleetSZ = len(allFleet)
        
        if(nowFleetSZ < self._lastFleetSZ):
            self._servingPool = {key: values for key, values in self._servingPool.items()
                                    if key in allFleet}
            self._rebalancingPool = { key: values for key, values in self._rebalancingPool.items()  
                                    if key in allFleet}
        
        self._lastFleetSZ = nowFleetSZ

        # 将_servingPool中刚完成订单任务的车辆对应记录删除
        oldServingPool = self._servingPool
        self._servingPool = { key: values for key, values in self._servingPool.items() 
                                if key not in emptyFleet }
        
        for key in oldServingPool.keys() - self._servingPool.keys():
            predArrivalTime = oldServingPool[key].estArrivalTime
            actArrivalTime = now
            self._logprinter.updateServicePredRecord(key, predArrivalTime=predArrivalTime, 
                                                    actArrivalTime=actArrivalTime)
        # 将_rebalancingPool中完成再平衡任务的车辆对应记录删除
        oldRebalancingPool = self._rebalancingPool
        self._rebalancingPool = { key: values for key, values in self._rebalancingPool.items()  
                                if key in emptyFleet and traci.vehicle.getStopState(key) == 0 }
        
        finishedRebalanceVehs = set(oldRebalancingPool.keys()) - set(self._rebalancingPool.keys())
        for vID in finishedRebalanceVehs:
            # arrivalTime有至多StepTime的误差
            self._dataToPrint['rebalanceinfo'].append({'vehID': vID, 'isDepart': False, 'arrivalTime': now})

        # 删除emptyFleet中同时在_rebalancingPool里出现过的车辆ID删除
        emptyFleet = [vID for vID in emptyFleet if vID not in self._rebalancingPool.keys()]
        emptyFleet = set(emptyFleet)

        for vID in emptyFleet - self._lastTimeVacantFleet:
            # 空闲车辆用绿色highlight
            traci.vehicle.highlight(vehID=vID, color=GREEN, size=HIGHLIGHTSZ, type=1)
        
        # 记录各状态出租车数量
        self._dataToPrint['taxistatnuminfo'] = {'time': now, 'vacantNum': len(emptyFleet), 
                                                'servingNum': len(self._servingPool), 
                                                'rebalancingNum': len(self._rebalancingPool)}

            
        print('vacant车辆数：')
        print(len(emptyFleet))
        print('serving车辆数：')
        print(len(self._servingPool))
        print('rebalancing车辆数：')
        print(len(self._rebalancingPool))
        print('总车辆数：')
        print(nowFleetSZ)
        

        ### 需求信息收集
        newReservations = traci.person.getTaxiReservations(1)
        # 将新到达的订单放入_openReservationPool
        for res in newReservations:
            assert res not in self._openReservationPool
            self._openReservationPool[res.id] = res
        
        print('开放订单数：')
        print(len(self._openReservationPool))
        assert nowFleetSZ == len(emptyFleet) + len(self._servingPool) + len(self._rebalancingPool)

        
        # 记录预测区间内各个区域的累计需求到达数
        for res in newReservations:
            pickupEdge = res.fromEdge
            pickupPos = res.departPos
            # if res.persons[0] == "43":
            #     print()
            x, y = traci.simulation.convert2D(edgeID=pickupEdge, pos=pickupPos, toGeo=True)
            inRegion = self._regionProjector.inWhichRegion(x, y, isCache=True)
            if inRegion is not None:
                self._regionDemandRecord[inRegion] += 1
            else:
                self._outOfRegionBoundTimes += 1
        
        # 记录本step各区域的实时未满足需求量
        nowRegionDemand = dict.fromkeys(self._centralEdges.keys(), 0)
        for resID, resObj in self._openReservationPool.items():
            pickupEdge = resObj.fromEdge
            pickupPos = resObj.departPos
            x, y = traci.simulation.convert2D(edgeID=pickupEdge, pos=pickupPos, toGeo=True)
            inRegion = self._regionProjector.inWhichRegion(x, y, isCache=True)
            if inRegion is not None:
                nowRegionDemand[inRegion] += 1
            else:
                pass
        
        self._dataToPrint['regionDemandSupplyRealtimeInfo'] = {'time': now, 'regionD': nowRegionDemand}
        
        
        ### 供给信息收集
        # 记录本step各区域的可用空车数
        nowRegionSupply = dict.fromkeys(self._centralEdges.keys(), 0)
        newVancantFleet = emptyFleet - self._lastTimeVacantFleet
        nowSDic = dict.fromkeys(self._centralEdges.keys())
        for key in nowSDic.keys():
            nowSDic[key] = set()
        for vID in emptyFleet:
            # 空闲车辆当前地理坐标(x, y)
            vX0, vY0 = traci.vehicle.getPosition(vehID=vID)
            vX0, vY0 = traci.simulation.convertGeo(vX0, vY0, fromGeo=False)
            # 找出车辆所在位置对应区域
            regionID = self._regionProjector.inWhichRegion(x=vX0, y=vY0, isCache=True)
            if regionID is not None:
                nowRegionSupply[regionID] += 1
                nowSDic[regionID].add(vID)
                if vID in newVancantFleet:
                    self._regionSupplyRecord[regionID] += 1
            else:
                pass
        
        self._dataToPrint['regionDemandSupplyRealtimeInfo']['regionS'] = nowRegionSupply

        # 记录各区域供需信息
        if now % DSAGGREGATEWINDOW == 0 and now > 0:
            self._dataToPrint['regionDemandSupplyAggregateInfo']['startTime'] = now - DSAGGREGATEWINDOW
            self._dataToPrint['regionDemandSupplyAggregateInfo']['timeSpan'] = DSAGGREGATEWINDOW
            self._dataToPrint['regionDemandSupplyAggregateInfo']['regionD'] = self._regionDemandRecord
            self._dataToPrint['regionDemandSupplyAggregateInfo']['regionS'] = self._regionSupplyRecord
            # 需求记录清零
            self._regionDemandRecord = dict.fromkeys(self._centralEdges.keys(), 0)
            # 以每个区域当前空车数初始化供给记录
            self._regionSupplyRecord = dict.fromkeys(self._centralEdges.keys(), 0)
            for regionID in self._regionSupplyRecord.keys():
                self._regionSupplyRecord[regionID] = len(nowSDic[regionID])
            


        ### 再平衡决策
        decision, vIDFRegion = self._rebalance(now, vacantFleet=emptyFleet, nowVehDic=nowSDic)
        
        if WITHREBALANCE:
            setOutVehCnt =  self._excecuteRebalance(now, emptyFleet=emptyFleet, decision=decision, vehIDFromRegion=vIDFRegion)
            self._rebalanceWindowRVCnt += setOutVehCnt
            print('本平衡调度时间窗内累计发出平衡车辆数：')
            print('\t已发：%d' % (self._rebalanceWindowRVCnt))
            print('\t目标:%d' % (self._rebalanceWindowRVCntObj))
            print()


        ### 决策（匹配）
        
        if DISPATCHALGO == 'GreedyClosest':
            decision = self._dispatchGreedyCloestEuclidean(vacantFleet=emptyFleet)
        elif DISPATCHALGO == 'MinCostFlow' or DISPATCHALGO == 'Hungarian':
            decision = self._dispatchSimpleAssignment(vacantFleet=emptyFleet)
        elif DISPATCHALGO == 'TestShared':
            decision = self._dispatchTestShared(vacantFleet=emptyFleet)
        elif DISPATCHALGO == 'InsertionHeuristic':
            begin = time.time()
            decision = self._dispatchInsertionHeuristic(now=now,vacantFleet=emptyFleet)
            end = time.time()
            print(f'决策+派单任务下达时间：%f s' % (end - begin))
        else:
            exit('dispatch decision')
        


        ### 得到决策结果后
        if DISPATCHALGO == 'TestShared':
            self._excecuteDispatch(now, emptyFleet=emptyFleet, decision=decision, isShared=True)
        else: 
            self._excecuteDispatch(now, emptyFleet=emptyFleet, decision=decision)

        # 更新nowSDic
        for key, value in nowSDic.items():
            nowSDic[key] = value & emptyFleet

        self._lastTimeVacantFleet = emptyFleet
        

        self._printLog(now)


        end0 =time.time()
        print(f'本step运行总时间：%f s' % (end0 - begin0))
        
        PoolingTaskInfo.stepFinish()
        return 0
            

    def _predictDemand(self, now, timeSpan) -> dict:
        pred = self._predictor.getPrediction(now,timeSpan)
        result = dict.fromkeys(self._centralEdges.keys(), 0)
        for key in result.keys():
            if key in pred.keys():
                result[key] = pred[key]
            
        return pred 

    def _excecuteRebalance(self, now, emptyFleet, decision, vehIDFromRegion):
        # 再平衡
        successTimes = 0
        for vID, regionID in decision.rebalance.items():
            actualEdge = self._centralEdges[regionID]
            stops = traci.vehicle.getStops(vID)
            assert len(stops) == 1
            originalStopData = stops[0]
            vehNowEdge = traci.lane.getEdgeID(originalStopData.lane)
            
            try:
                if vehNowEdge == actualEdge:
                    continue
                traci.vehicle.changeTarget(vID, actualEdge)
                obj1 = traci.vehicle.getStops(vID)
                if len(obj1) > 0:
                    assert len(obj1) == 1
                    traci.vehicle.replaceStop(vID, 0, '')              
                traci.vehicle.setStop(vehID=vID, flags=STOPFLAG, edgeID=actualEdge)
            except Exception as ex:
                print(ex)
                exit('再平衡')

            # 计算并记录预计到达时间
            # mode = traci.vehicle.getRoutingMode(vehID=vID)
            edges = traci.vehicle.getRoute(vID)
            estEnrouteTime = 0
            for edge in edges:
                traveltime = float((traci.vehicle.getParameter(vID, f"device.rerouting.edge:%s" % (edge))))
                assert traveltime != -1
                estEnrouteTime += traveltime
            
            routeLength = traci.vehicle.getDrivingDistance(vehID=vID, edgeID=actualEdge, pos=1.0)
            
            self._rebalancingPool[vID] = EnrouteInfo(desArea=regionID, estArrivalTime=(now + estEnrouteTime))

            self._dataToPrint['rebalanceinfo'].append({'isDepart': True, 'departTime': now, 'vehID': vID, 
                                                       'fromRegion': vehIDFromRegion[vID], 'toRegion': regionID, 
                                                       'fromEdge':vehNowEdge, 'toEdge': actualEdge, 
                                                       'routeLength': routeLength, 'predDuration': estEnrouteTime})
            # 平衡车辆用棕色highlight
            traci.vehicle.highlight(vehID=vID, color=BROWN, size=HIGHLIGHTSZ)
            emptyFleet.remove(vID)
            successTimes += 1
        return successTimes


    def _excecuteDispatch(self, now, emptyFleet, decision, isShared=False):
        # 订单匹配
        for vID, value in decision.service.items():
            if not isShared:
                resID = value
                resStage = self._openReservationPool[resID]
                isSuccess = PoolingTaskInfo.changeTaskQ(resObj=resStage, vID=vID)
                
                if not isSuccess:
                    continue

                ### 派单成功
                # 订单目的地的地理坐标(x, y)
                x, y = traci.simulation.convert2D(edgeID=resStage.toEdge, pos=resStage.arrivalPos, toGeo=True)
                # 目的地区域ID
                regionID = self._regionProjector.inWhichRegion(x=x, y=y)
                
                self._servingPool[vID] = EnrouteInfo(desArea=regionID, estArrivalTime=-1)

                # 从open订单池中删除相应订单记录
                self._openReservationPool.pop(resID)
                # 服务车辆用红色highlight
                traci.vehicle.highlight(vehID=vID, color=RED, size=HIGHLIGHTSZ)
                emptyFleet.remove(vID)
            else:
                # 拼车
                resID = value['resID']
                pickPos, dropPos = value['insertPositions']
                resStage = self._openReservationPool[resID]
                isSuccess = PoolingTaskInfo.changeTaskQ(resObj=resStage, vID=vID, 
                                                        pickupInsertPos=pickPos, dropoffInsertPos=dropPos)
                if not isSuccess:
                    continue
                
                ### 派单成功
                # desResID = PoolingTaskInfo.poolingRecords[vID].taskQ[-1]    # 终点订单id
                desResID = PoolingTaskInfo.getPoolingRecordValue(vID).taskQ[-1]    # 终点订单id
                
                desResObj = PoolingTaskInfo.getClosedResRecord(desResID)
                
                
                # 订单目的地的地理坐标(x, y)
                x, y = traci.simulation.convert2D(edgeID=desResObj.toEdge, pos=desResObj.arrivalPos, toGeo=True)
                # 目的地区域ID
                regionID = self._regionProjector.inWhichRegion(x=x, y=y)
                self._servingPool[vID] = EnrouteInfo(desArea=regionID, estArrivalTime=-1)
                
                # 从open订单池中删除相应订单记录
                self._openReservationPool.pop(resID)

                if vID in emptyFleet:
                    # 服务车辆用红色highlight
                    traci.vehicle.highlight(vehID=vID, color=RED, size=HIGHLIGHTSZ)
                    emptyFleet.remove(vID)


    def _rebalance(self, now, vacantFleet, nowVehDic):
        # 决策阶段
        if now % REBALANCETIME == 0:
            self._rebalanceWindowRVCnt = 0
            ### 计算Cij
            pre_value = self._predictDemand(now, PREDTIME)
            self._regionalCosts = {}
            # 初始化_rebalanceLenRank
            self._rebalanceLenRank = {}
            regionIDLst = list(self._centralEdges.keys())

            start = time.time()
            print("区域间路径计算：")
            print("...")
            
            for regionID1 in regionIDLst:
                self._rebalanceLenRank[regionID1] = []
                for regionID2 in regionIDLst:
                    if regionID1 != regionID2:
                        centerEdge1 = self._centralEdges[regionID1]
                        centerEdge2 = self._centralEdges[regionID2]
                        stgRou = traci.simulation.findRoute(fromEdge=centerEdge1, toEdge=centerEdge2, 
                                                            vType='taxi', depart=-1, 
                                                            routingMode=traci.constants.ROUTING_MODE_AGGREGATED, )
                        self._regionalCosts[(regionID1, regionID2)] = stgRou.travelTime

                        # 以每个区域到其他区域的距离大小进行排序（由大到小）
                        foundIndex = len(self._rebalanceLenRank[regionID1])
                        for idx, regionID3 in enumerate(self._rebalanceLenRank[regionID1]):
                            if self._regionalCosts[(regionID1, regionID3)] < stgRou.length:
                                foundIndex = idx
                                break
                        self._rebalanceLenRank[regionID1].insert(foundIndex, regionID2)
            end = time.time()                        
            print(f"计算完毕，用时：%d s"%(end - start))
        
            ### si
            latestBound = now + PREDTIME
            # 未来供应量
            futureSupply = self._predictFutureSupply()
            # 未来+现在供应量
            sDict = futureSupply.copy()

            for regionID in sDict.keys():
                sDict[regionID] += len(nowVehDic[regionID])

            ### λi
            lambdaDict = pre_value

            # log文件数据
            self._dataToPrint['regionDemandSupplyPredInfo']['startTime'] = now
            self._dataToPrint['regionDemandSupplyPredInfo']['timeSpan'] = PREDTIME
            self._dataToPrint['regionDemandSupplyPredInfo']['regionD'] = pre_value
            self._dataToPrint['regionDemandSupplyPredInfo']['regionS'] = sDict

            result = self._rebalanceModel.decide(now, regionalCosts=self._regionalCosts, regionSupplyRates=sDict, regionResArrivalRates=lambdaDict)
            # self._rebalanceTasks = result
            self._rebalanceTasks = dict.fromkeys(self._centralEdges.keys())
            # 生成平衡任务
            # debug
            rcnt = 0 
            for fromRegionID in self._rebalanceTasks.keys():
                self._rebalanceTasks[fromRegionID] = {'target': [0, 0.0], 'real': [0, 0], 'taskLst': [], 'stepLen': -1}
                totRebalanceVehCnt = 0
                for toRegionID in self._rebalanceLenRank[fromRegionID]:
                    # 计划在平衡调度区间内要从fromRegionID调rFromTo辆车到toRegionID
                    if toRegionID == fromRegionID:
                        continue
                    rFromTo = int(result[(fromRegionID, toRegionID)])
                    if rFromTo > 0:
                        # 如果>0，就安排这个任务（根据路程距离由远到近的排序）
                        rcnt += rFromTo
                        totRebalanceVehCnt += rFromTo
                        self._rebalanceTasks[fromRegionID]['taskLst'].append((toRegionID, rFromTo))
                
                # 每step要完成的任务量
                self._rebalanceTasks[fromRegionID]['stepLen'] = (totRebalanceVehCnt / REBALANCETASKTIME) * STEPTIME
            
            self._rebalanceTasks = dict(item for item in self._rebalanceTasks.items() if len(item[1]['taskLst']) > 0)
            self._rebalanceWindowRVCntObj = rcnt 
            return Decision({}, {}), {}
        else:

            rebalanceDecision = {}
            vIDFRegion = {}
            for fRegionID in self._rebalanceTasks.keys():
                # 移动target指针
                stepLen = self._rebalanceTasks[fRegionID]['stepLen']
                targetPos = self._rebalanceTasks[fRegionID]['target'][0]
                targetCompleted = self._rebalanceTasks[fRegionID]['target'][1]
                realPos = self._rebalanceTasks[fRegionID]['real'][0]
                realCompleted = self._rebalanceTasks[fRegionID]['real'][1]
                taskLst = self._rebalanceTasks[fRegionID]['taskLst']
                
                if targetPos != len(taskLst):
                    accum = 0
                    accum += taskLst[targetPos][1] - targetCompleted
                    while accum < stepLen:
                        targetPos += 1
                        targetCompleted = 0
                        if targetPos == len(taskLst):
                            break
                        accum += taskLst[targetPos][1] - targetCompleted
                    else:
                        if targetPos == len(taskLst):
                            break
                        targetCompleted = taskLst[targetPos][1] - (accum - stepLen)
                        if targetCompleted == taskLst[targetPos][1]:
                            targetPos += 1
                            targetCompleted = 0
                    # 更新
                    self._rebalanceTasks[fRegionID]['target'][0] = targetPos
                    self._rebalanceTasks[fRegionID]['target'][1] = targetCompleted
                

                if realPos != len(taskLst):
                    # accum = 0
                    # accum += taskLst[targetPos][1] - targetCompleted
                    while len(nowVehDic[fRegionID]) > 0 and (realPos != targetPos or (realPos == targetPos and realCompleted < int(targetCompleted))):
                        toRegionID = taskLst[realPos][0]
                        if realPos != targetPos:
                            num1 = taskLst[realPos][1] - realCompleted
                            if len(nowVehDic[fRegionID]) >= num1:
                                for _ in range(num1):
                                    vID = nowVehDic[fRegionID].pop()
                                    rebalanceDecision[vID] = toRegionID
                                    vIDFRegion[vID] = fRegionID
                                ##到达最大临界值进一，记得！！    
                                realPos += 1
                                realCompleted = 0
                            else:
                                num2 = len(nowVehDic[fRegionID])
                                for _ in range(num2):
                                    vID = nowVehDic[fRegionID].pop()
                                    rebalanceDecision[vID] = toRegionID
                                    vIDFRegion[vID] = fRegionID
                                realCompleted += num2
                        else:
                            # realPos == targetPos and realCompleted < int(targetCompleted)的情况
                            gap = int(targetCompleted) - realCompleted
                            sendout = min(gap, len(nowVehDic[fRegionID]))

                            for _ in range(sendout):
                                vID = nowVehDic[fRegionID].pop()
                                rebalanceDecision[vID] = toRegionID
                                vIDFRegion[vID] = fRegionID
                            realCompleted += sendout
                        
                    # 更新指针记录
                    self._rebalanceTasks[fRegionID]['real'][0] = realPos
                    self._rebalanceTasks[fRegionID]['real'][1] = realCompleted
                    
            return Decision({}, rebalanceDecision), vIDFRegion

    def _predictFutureSupply(self):
        with open(r'data/temp/curr_routes.rou.xml', 'w+') as rouFile:
            sumolib.xml.writeHeader(outf=rouFile, root='routes')
            # rouFile.write("<routes>\n")
            for vID, enrouteInfo in (self._servingPool | self._rebalancingPool).items():
                typeID = 'taxi'
                route = traci.vehicle.getRoute(vehID=vID)
                
                # routeID = 'initRoute_' + vID
                fEdgeIndex = traci.vehicle.getRouteIndex(vehID=vID)
                if fEdgeIndex == -1:
                    fEdgeIndex = 0
                routeStr = ''
                
                for idx, edge in enumerate(route):
                    if idx < fEdgeIndex:
                        continue
                    routeStr +=  f'%s' % (edge)
                    if idx != len(route) - 1:
                        routeStr += ' '
                
                stops = traci.vehicle.getStops(vehID=vID)
                assert len(stops) > 0
                lastStop = stops[-1]

                # debug
                if len(stops) == 1:
                    pass
                          
                rouFile.write("""\t<vehicle id="%s" depart="%s">\n """
                                % (vID, str(0)) )
                rouFile.write("""\t\t<route edges="%s">\n"""%(routeStr)) 

                # rouFile.write("""\t\t\t<stop lane="%s" endPos="%s" actType="%s" parking="true"/>\n"""%(str(lastStop.lane), str(lastStop.endPos), str(lastStop.actType)))

                rouFile.write("""\t\t</route>\n""")   

                rouFile.write("""\t</vehicle>\n""")
            rouFile.write("</routes>\n")
            rouFile.close()
        
        sumoBinary = checkBinary('sumo')

        subprocess.call([sumoBinary, '-n', r'data/sumo_inputs/convert-1.net.xml',
                            '--additional-files', r'data/sumo_inputs/type.add.xml', 
                            '--route-files', r'data/temp/curr_routes.rou.xml', 
                            # '--device.taxi.idle-algorithm', 'randomCircling',
                            '--tripinfo-output.write-unfinished',
                            '--vehroute-output', r'data/temp/resultroutes.rou.xml',
                            '--vehroute-output.write-unfinished', 
                            # '--tripinfo-output', r'data/temp/tripinfos.xml', 
                            '--stop-output', r'data/temp/stopinfo.xml',
                            '--vehroute-output.cost',
                            '--vehroute-output.exit-times',
                            '--gui-settings-file', r'data/sumo_inputs/gui-settings.xml',
                            # '--vehroute-output.dua',
                            "--tripinfo-output",
                            r"data/temp/tripinfos.xml",
                            '--end', str(PREDTIME)])

        futureSupply = dict.fromkeys(self._centralEdges.keys(), 0)
        for vID, arrival in sumolib.xml.parse_fast(r'data/temp/resultroutes.rou.xml', 'vehicle', ['id', 'arrival']):
            
            if vID in self._servingPool.keys():
                desArea = self._servingPool[vID].desArea
            elif vID in self._rebalancingPool.keys():
                desArea = self._rebalancingPool[vID].desArea
            else:
                raise Exception('_predictFutureSupply')
            if desArea is not None:
                futureSupply[desArea] += 1
            
        return futureSupply

    def _dispatchSimpleAssignment(self, vacantFleet):
        vIDLst = list(vacantFleet)
        resIDLst = list(self._openReservationPool.keys())
        timeCostVRLst = []

        begin = time.time()
        fromToLst = []
        tripID = 0
        mapRec = {}
        for i, vID in enumerate(vIDLst):
            stops = traci.vehicle.getStops(vID)
            assert len(stops) == 1
            originalStopData = stops[0]
            vehNowEdge = traci.lane.getEdgeID(originalStopData.lane)
            for j, resID in enumerate(resIDLst):
                resObj = self._openReservationPool[resID]
                fromToLst.append((vehNowEdge, resObj.fromEdge))
                mapRec[tripID] = (i, j, vID, resID)
                tripID += 1
        
        routesCosts = Router.getRoutes(fromToLst)


        timeCostVRLst = [[BIGINT for x in range(len(resIDLst))] for y in range(len(vIDLst))] 

        for tripID, cost in routesCosts.items():
            i, j, vID, resID = mapRec[tripID]
            timeCostVRLst[i][j] = int(cost * 100)

        end = time.time()
        print(f'司乘间最短路径计算时间%f' % (end - begin))
        result = {}
        begin = time.time()
        if len(vIDLst) > 0:
            if DISPATCHALGO == 'MinCostFlow':
                result = self._dispatchModel.decideMinFlowCostNew(vIDLst, resIDLst, timeCostVRLst)
            elif DISPATCHALGO == 'Hungarian':
                result = self._dispatchModel.decideHungarian(vIDLst, resIDLst, timeCostVRLst)
            else:
                raise Exception('_dispatchSimpleAssignment')
        end = time.time()
        print(f'派单决策时间：%f s' % (end - begin))
        service = result
        return Decision(service, {})
 

    def _dispatchGreedyCloestEuclidean(self, vacantFleet):
        vIDLst = list(vacantFleet)
        resIDLst = list(self._openReservationPool.keys())
        
        distanceVRLst = []
        
        vEdgePos = {}
        for vID in vIDLst:
            vX0, vY0 = traci.vehicle.getPosition(vehID=vID)
            vEdgePos[vID] = {'coord': (vX0, vY0)}

        resEdgePosEnterTime = {}
        for resID in resIDLst: 
            toRes = []
            res = self._openReservationPool[resID]
            resAtEdge = res.fromEdge
            resAtPos = res.departPos
            rX, rY = traci.simulation.convert2D(resAtEdge, resAtPos, toGeo=False)
            resEnterTime = res.depart
            resEdgePosEnterTime[resID] = {'edge': resAtEdge, 'pos': resAtPos, 'coord': (rX, rY), 'enterTime': resEnterTime}

        # 先来先服务
        resIDLst = sorted(resIDLst, key=lambda resID: resEdgePosEnterTime[resID]['enterTime'])

        # 算出每个订单和每辆车之间的曼哈顿距离
        for resID in resIDLst:
            toRes = []
            for vID in vIDLst:
                vX, vY = vEdgePos[vID]['coord']
                rX, rY = resEdgePosEnterTime[resID]['coord']
                dist = abs(vX - rX) + abs(vY- rY)
                toRes.append(dist)
            distanceVRLst.append(toRes)

        distanceVRArr = np.array(distanceVRLst)
        matchMatrix = np.zeros([len(resIDLst), len(vIDLst)])
        i = 0
        assignedVCnt = 0
        
        while i < len(resIDLst):
            if assignedVCnt == len(vIDLst):
                break
            toRes = distanceVRArr[i] 
            result = np.where(toRes == np.amin(toRes)) 
            j = result[0][0]
            matchMatrix[i][j] = 1
            distanceVRArr[:, j] = np.inf
            i += 1
            assignedVCnt += 1
            # if assignedVCnt == len(vIDLst):
            #     break
            
        service = {}
        rebalance = {}

        for i in range(len(resIDLst)):
            for j in range(len(vIDLst)):
                if matchMatrix[i][j] == 1:
                    matchedResID = resIDLst[i]
                    matchedVID = vIDLst[j]
                    service[matchedVID] = matchedResID
        
        return Decision(service=service, rebalance=rebalance)
    
    # 测试拼车功能
    def _dispatchTestShared(self, vacantFleet:set):
        ran = random.Random()
        service = {}
        vacantFleet = copy.deepcopy(vacantFleet)    # 副本
        for resID, resObj in self._openReservationPool.items():
            if PoolingTaskInfo.getPoolingRecordsLen() == 0:
                if len(vacantFleet) > 0:
                    vID = random.choice(list(vacantFleet))
                    service[vID] = {'resID': resID, 'insertPositions': (0, 0)}
                    vacantFleet.remove(vID)
            else:    
                vID, poolingInfo = random.choice(PoolingTaskInfo.getPoolingRecordItems())
                tq = poolingInfo.taskQ
                if len(set(tq)) < traci.vehicle.getPersonCapacity(vID):
                    pickPos = poolingInfo.currPos + ran.randint(0, len(tq) - poolingInfo.currPos)
                    dropPos = pickPos + ran.randint(0, len(tq) - pickPos)
                    service[vID] = {'resID': resID, 'insertPositions': (pickPos, dropPos)}
                else:
                    if len(vacantFleet) > 0:
                        vID = random.choice(list(vacantFleet))
                        service[vID] = {'resID': resID, 'insertPositions': (0, 0)}
                        vacantFleet.remove(vID)

        return Decision(service, {})
    
    def _dispatchInsertionHeuristic(self, now, vacantFleet:set):
        # 调用duarouter求出各辆车直接接客的最短时间+目前任务的剩下时间

        begin = time.time()
        fromToLst = []
        tripID = 0
        mapRec = {}
        vIDLst = list(vacantFleet)
        for i, vID in enumerate(vIDLst):
            stops = traci.vehicle.getStops(vID)
            assert len(stops) == 1
            originalStopData = stops[0]
            vehOnEdge = traci.lane.getEdgeID(originalStopData.lane)
            for j, resID in enumerate(list(self._openReservationPool.keys())):
                resObj = self._openReservationPool[resID]
                fromToLst.append((vehOnEdge, resObj.fromEdge))
                mapRec[tripID] = (vehOnEdge, resObj.fromEdge, vID, resID)
                tripID += 1
        
        # serving车辆到下一个任务点的剩下的时间，以及改变路线去接新订单的用时
        for vID in PoolingTaskInfo.getPoolingRecordKeys():
            # test
            route = traci.vehicle.getRoute(vehID=vID)
            rIdx = traci.vehicle.getRouteIndex(vID)
            edgePFore = route[rIdx]

            vehOnEdge = traci.vehicle.getRoadID(vID)
            if vehOnEdge == '' or vehOnEdge[0] == ':':
                route = traci.vehicle.getRoute(vehID=vID)
                rIdx = traci.vehicle.getRouteIndex(vID)
                vehOnEdge = route[rIdx]
            else:
                assert edgePFore == vehOnEdge
            
            # 下一个任务
            info = PoolingTaskInfo.getPoolingRecordValue(vID)
            nextEdge, _ = PoolingTaskInfo.getTaskPointEdge(info.taskQ, info.currPos)
            fromToLst.append((vehOnEdge, nextEdge))
            mapRec[tripID] = (vehOnEdge, nextEdge, vID, info.taskQ[info.currPos])
            tripID += 1

            #  改变路线去接新订单
            for j, resID in enumerate(list(self._openReservationPool.keys())):
                resObj = self._openReservationPool[resID]
                fromToLst.append((vehOnEdge, resObj.fromEdge))
                mapRec[tripID] = (vehOnEdge, resObj.fromEdge, vID, resID)
                tripID += 1
            
        routesCosts = Router.getRoutes(fromToLst)

        timeCostVEREDict = dict.fromkeys(fromToLst, float('inf'))
        

        for tripID, cost in routesCosts.items():
            vehOnEdge, toEdge, vID, resID = mapRec[tripID]
            timeCostVEREDict[(vehOnEdge, toEdge)] = float(cost)

        end = time.time()
        print(f'司乘间最短路径计算时间%f' % (end - begin))

        class VehInsertionInfo(object):
            def __init__(self, vID, isEmpty) -> None:
                self.currPos: int
                self.taskQueue: list
                self.capacity: int
                self.oldEdges: list
                self.boardInfo: list
                self.vID = vID

                if isEmpty is False:
                    info:PoolingTaskInfo = PoolingTaskInfo.getPoolingRecordValue(key=vID)
                    self.currPos = info.currPos
                    self.taskQueue = info.taskQ
                    self.capacity = traci.vehicle.getPersonCapacity(vID)

                    # -----test-----
                    route = traci.vehicle.getRoute(vehID=vID)
                    rIdx = traci.vehicle.getRouteIndex(vID)
                    edgePFore = route[rIdx]
                    # -----test-----

                    vehOnEdge = traci.vehicle.getRoadID(vID)
                    if vehOnEdge == '' or vehOnEdge[0] == ':':
                        route = traci.vehicle.getRoute(vehID=vID)
                        rIdx = traci.vehicle.getRouteIndex(vID)
                        vehOnEdge = route[rIdx]
                    else:
                        assert edgePFore == vehOnEdge
                    self.oldEdges = [vehOnEdge]
                    self.boardInfo = [len(traci.vehicle.getPersonIDList(vID))]

                    for pos, task in enumerate(self.taskQueue):
                        if pos < self.currPos:
                            continue
                        atPosEdge, appearTimes = PoolingTaskInfo.getTaskPointEdge(self.taskQueue, pos)
                        self.oldEdges.append(atPosEdge)
                        personNum = len(PoolingTaskInfo.getClosedResRecord(task).persons)
                        if appearTimes == 0:
                            # 上车
                            self.boardInfo.append(personNum)
                        elif appearTimes == 1:
                            # 下车
                            self.boardInfo.append(-personNum)
                        else:
                            raise Exception('appearTimes')
                else:
                    # 空车
                    self.currPos = 0
                    self.taskQueue = []
                    self.capacity = traci.vehicle.getPersonCapacity(vID)
                    # -----test-----
                    route = traci.vehicle.getRoute(vehID=vID)
                    rIdx = traci.vehicle.getRouteIndex(vID)
                    edgePFore = route[rIdx]
                    # -----test-----

                    vehOnEdge = traci.vehicle.getRoadID(vID)
                    if vehOnEdge == '' or vehOnEdge[0] == ':':
                        route = traci.vehicle.getRoute(vehID=vID)
                        rIdx = traci.vehicle.getRouteIndex(vID)
                        vehOnEdge = route[rIdx]
                    else:
                        assert edgePFore == vehOnEdge
                    self.oldEdges = [vehOnEdge]
                    self.boardInfo = [0]
 

            def _getCost(self, edges):
                # 列表首个元素必须是车辆所在边
                sumCost = 0.0
                times = len(edges) - 1
                for fi in range(len(edges) - 1):
                    if fi == 0:
                        sumCost += timeCostVEREDict[(edges[fi], edges[fi + 1])] * times
                    else:
                        sumCost += PoolingTaskInfo.getEdgePairTimeCost(edges[fi], edges[fi + 1]) * times
                    times -= 1
                return sumCost                
            
            def getIOldCost(self):
                return self._getCost(self.oldEdges)
            
            def getNewCost(self, insertPickPos, pickEdge, insertDropPos, dropEdge, personNum):
                adjustedInsertPos = [insertPickPos - self.currPos + 1, insertDropPos - self.currPos + 1]

                # 检查容量限制
                newBoardInfo = copy.deepcopy(self.boardInfo)

                # 上车点插入
                newBoardInfo.insert(adjustedInsertPos[0], personNum)
                adjustedInsertPos[1] += 1
                # 下车点插入
                newBoardInfo.insert(adjustedInsertPos[1], -personNum)

                onboardNum = newBoardInfo[0]
                for i in range(1, len(newBoardInfo)):
                    onboardNum += newBoardInfo[i]
                    assert onboardNum >= 0
                    if onboardNum > self.capacity:
                        # 插入策略违反容量限制
                        return float('inf')
                
                # 返回新cost
                newEdges = copy.deepcopy(self.oldEdges)
                newEdges.insert(adjustedInsertPos[0], pickEdge)
                newEdges.insert(adjustedInsertPos[1], dropEdge)

                return self._getCost(newEdges)
            
            # 返回最佳插入位置以及其边际成本
            def getBestInsertion(self, pickEdge, dropEdge, personNum):
                oldCost = self.getIOldCost()
                if len(self.taskQueue) == 0:
                    # 空车
                    return (0, 0) , self.getNewCost(0, pickEdge, 0, dropEdge, personNum) - oldCost
                else:
                    # serving车辆
                    bestInsertPos = None
                    bestNewCost = None
                    for i in range(self.currPos, len(self.taskQueue)):
                        for j in range(i, len(self.taskQueue)):
                            newCost = self.getNewCost(i, pickEdge, j, dropEdge, personNum)
                            if bestNewCost is None and newCost != float('inf'):
                                bestInsertPos = (i, j)
                                bestNewCost = newCost
                    if bestNewCost is not None:
                        return bestInsertPos, bestNewCost - oldCost
                    else:
                        return None, None
                

        # 先来先服务
        resIDLst = list(self._openReservationPool.keys())
        resIDLst = sorted(resIDLst, key=lambda resID: self._openReservationPool[resID].depart)
        for resID in resIDLst:
            
            resObj = self._openReservationPool[resID]
            # 准备算法用到的vehInsertionInfo
            vInfoLst = []
            for vID in PoolingTaskInfo.getPoolingRecordKeys():
                vInfoLst.append(VehInsertionInfo(vID=vID, isEmpty=False))
            
            for vID in vacantFleet:
                vInfoLst.append(VehInsertionInfo(vID=vID, isEmpty=True))

            bestScheme = {'vehID': None, 'insertion': None, 'marginalCost': None}

            for info in vInfoLst:
                # info = VehInsertionInfo(info)
                insertion, marginalCost = info.getBestInsertion(resObj.fromEdge, resObj.toEdge, len(resObj.persons))
                if marginalCost is not None:
                    if bestScheme['marginalCost'] is not None:
                        if marginalCost < bestScheme['marginalCost']:
                            bestScheme['vehID'] = info.vID
                            bestScheme['insertion'] = insertion
                            bestScheme['marginalCost'] = marginalCost
                    else:
                        bestScheme['vehID'] = info.vID
                        bestScheme['insertion'] = insertion
                        bestScheme['marginalCost'] = marginalCost
            
            if bestScheme['marginalCost'] is not None:
                service = {}
                service[bestScheme['vehID']] =  {'resID': resID, 'insertPositions': bestScheme['insertion']}
                self._excecuteDispatch(now=now, emptyFleet=vacantFleet, 
                                        decision=Decision(service=service, rebalance={}), 
                                    isShared=True)                
                    
        return Decision({}, {})

                
    
    def _printLog(self, now):
        if self._dataToPrint['taxistatnuminfo'] is not None:
            info = self._dataToPrint['taxistatnuminfo']
            self._logprinter.updateTaxiStatNumRecord(time=info['time'], vacantNum=info['vacantNum'], 
                                                     servingNum=info['servingNum'], 
                                                     rebalancingNum=info['rebalancingNum'])
            self._dataToPrint['taxistatnuminfo'] = None

        if self._dataToPrint['regionDemandSupplyRealtimeInfo'] is not None:
            time = self._dataToPrint['regionDemandSupplyRealtimeInfo']['time']
            regionD = self._dataToPrint['regionDemandSupplyRealtimeInfo']['regionD']
            regionS = self._dataToPrint['regionDemandSupplyRealtimeInfo']['regionS']
            self._logprinter.updateRealTimeEachRegionDemandSupply(time=time, regionD=regionD, regionS=regionS)
            self._dataToPrint['regionDemandSupplyRealtimeInfo'] = None
        
        if now % DSAGGREGATEWINDOW == 0 and now > 0:
            startTime = self._dataToPrint['regionDemandSupplyAggregateInfo']['startTime']
            timeSpan = self._dataToPrint['regionDemandSupplyAggregateInfo']['timeSpan']
            regionD = self._dataToPrint['regionDemandSupplyAggregateInfo']['regionD']
            regionS = self._dataToPrint['regionDemandSupplyAggregateInfo']['regionS']
            self._logprinter.updateAggregateEachRegionDemandSupply(startTime=startTime, timespan=timeSpan, regionD=regionD, regionS=regionS)
        
        if now % PREDTIME == 0:
            startTime = self._dataToPrint['regionDemandSupplyPredInfo']['startTime']
            timeSpan = self._dataToPrint['regionDemandSupplyPredInfo']['timeSpan']
            regionD = self._dataToPrint['regionDemandSupplyPredInfo']['regionD']
            regionS = self._dataToPrint['regionDemandSupplyPredInfo']['regionS']
            self._logprinter.updatePredEachRegionDemandSupply(startTime=startTime, timespan=timeSpan, regionD=regionD, regionS=regionS)
        
        if len(self._dataToPrint['rebalanceinfo']) > 0:
            for infoDic in self._dataToPrint['rebalanceinfo']:
                isDepart = infoDic['isDepart']
                if isDepart:
                    vehID = infoDic['vehID']
                    departTime = infoDic['departTime']
                    fromRegion = infoDic['fromRegion']
                    toRegion = infoDic['toRegion']
                    fromEdge = infoDic['fromEdge']
                    toEdge = infoDic['toEdge']
                    predDuration = infoDic['predDuration']
                    routeLength = infoDic['routeLength']
                    self._logprinter.updateRebalanceRecord(vehID=vehID, isDepart=True, departTime=departTime, fromRegion=fromRegion, 
                                                           toRegion=toRegion, fromEdge=fromEdge, toEdge=toEdge, 
                                                           routeLength=routeLength, predDuration=predDuration)
                else:
                    vehID = infoDic['vehID']
                    arrivalTime = infoDic['arrivalTime']
                    self._logprinter.updateRebalanceRecord(vehID=vehID, isDepart=isDepart, arrivalTime=arrivalTime)

            self._dataToPrint['rebalanceinfo'] = []

        self._logprinter.printout()

    def finish(self):
        print('仿真完成。')
        self._logprinter.finish()

