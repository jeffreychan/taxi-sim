from __future__ import absolute_import
from __future__ import print_function

import os
import subprocess
import sys
import optparse


# we need to import python modules from the $SUMO_HOME/tools directory

if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")

from sumolib import checkBinary  # noqa
import traci  # noqa
import sumolib


class Router0(object):

    def __init__(self, n:int) -> None:
        if n <= 0:
            raise Exception('Router.__init__')
        
        self._workers = []
        for i in range(n):
            net = sumolib.net.readNet(r'data/sumo_inputs/convert-1.net.xml')
            self._workers.append(net)

        
    def getRoutes(self, fromTo:list ,edgeCosts = {}, internal=False):
        # 生成trip file
        pass


class Router(object):

    def __init__(self) -> None:
        pass

        
    def getRoutes(fromTo:list , vType='taxi', threads=6, edgeCosts = {}, internal=False):
        # 生成trip file
        with open(r'data/temp/rou/dua_trips.rou.xml', 'w') as tripFile:
            sumolib.xml.writeHeader(outf=tripFile, root='routes')

            for idx, tup in enumerate(fromTo):
                fromEdge, toEdge = tup
                tripID = str(idx)
                tripFile.write(f'\t<trip id="%s" type="%s" depart="0.00" from="%s" to="%s"/>\n' 
                                % (tripID, vType, fromEdge, toEdge))
            tripFile.write('</routes>')
            tripFile.close()
        
        # 生成weight file
        # with open(r'data/temp/rou/edge_weights.add.xml', 'w') as weightFile:
        #     weightFile.close()
            
        # 运行duarouter
        sumoBinary = checkBinary('duarouter')
        subprocess.call([sumoBinary, '-n', r'data/sumo_inputs/convert-1.net.xml', 
                        '--additional-files', r'data/sumo_inputs/type.add.xml',
                        '--route-files', r'data/temp/rou/dua_trips.rou.xml',
                        '-o', r'data/temp/rou/dua_output.xml',
                        # '--weight-files', r'data/temp/rou/edge_weights.add.xml',
                        '--ignore-errors', 'true', '--no-warnings', 'true',
                        '--bulk-routing', 'true', '--routing-threads', str(threads)] 
                        )
        # parse travel time between edges
        result = {}
        for trip, route in sumolib.xml.parse_fast_nested(r'data/temp/rou/dua_output.alt.xml',
                                            "vehicle", "id", "route", "cost",
                                            optional=True):
            if route.cost:
                result[int(trip.id)] = float(route.cost)
        
        return result

        

if __name__ == "__main__":
    ftLst = [("3683", "-16211"), 
            ("16074", "1427")]
    print(Router.getRoutes(ftLst))
    